/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 02:53:21 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/23 19:24:44 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "png.h"

int					invalid_trns_chunk(t_img *img, int len)
{
	return ((img->color_type == 3 && len > img->plte_nb) || (img->
color_type == 2 && len != 3) || (!img->color_type && len != 2) || (img->
color_type == 4 || img->color_type == 6));
}

int					get_trns_info(t_img *img, unsigned char *tmp, int fd)
{
	int				i;
	int				len;
	unsigned char	c[6];

	len = chunk_len(tmp);
	i = -1;
	if (invalid_trns_chunk(img, len))
		return (0);
	if (img->color_type == 3)
		while (++i < img->plte_nb)
			if (read(fd, c, 1) != 1)
				return (0);
			else
				img->plte[i] |= (i < len ? (c[0] << 24) : (255 << 24));
	else if (img->color_type == 2)
		if ((read(fd, c, 6) != 6))
			return (0);
		else
			img->trns = ((uint8_t)(*c)) << 24 | ((uint8_t)(*(c + 2))) <<
16 | ((uint8_t)(*(c + 4))) << 8;
	else if (!img->color_type && ((read(fd, c, 2) != 2)))
		return (0);
	else if (!img->color_type)
		img->trns = ((uint16_t)(*c));
	return (1);
}

t_img				get_png_header_info(int fd, int len)
{
	t_img			img;
	unsigned char	data[len];
	int ret;

	ft_bzero(&img, sizeof(img));
	if ((ret = read(fd, data, len)) != len)
	{
		//read(fd, data, len - ret);
		//printf("RET M %d \n", ret);
		img.err = 1;
	}
	img.w = chunk_len(data);
	img.h = chunk_len(data + 4);
	if (((img.depth = *(data + 8)) != 8))
		img.err = 1;
	img.color_type = *(data + 9);
	((img.color_type == 4 || img.color_type == 0) ? img.err = 1 : 1);
	img.compression = *(data + 10);
	img.filter = *(data + 11);
	((img.compression || img.filter) ? img.err = 1 : 1);
	img.interlaced = *(data + 12);
	//(img.interlaced ? img.err = 1 : 1);
	if (img.color_type == 6 || img.color_type == 4)
		img.channels = (img.color_type == 6 ? 4 : 2);
	else
		img.channels = (!img.color_type || img.color_type == 3 ? 1 : 3);
	img.bpp = (img.depth / 8) * img.channels;
	img.linesize = 1 + (img.w * img.bpp);
	img.usize = img.linesize * img.h;
	img.fd = fd;
	return (img);
}

int					import_png_to_surface(int fd, t_img *img)
{
	unsigned char	tmp[8];
	uint32_t		comp_length;
	unsigned char	*zip;

	comp_length = 0;
	if (!fd || !(img->surf->pixels) || read(fd, tmp, 8) < 8)
		return (0);
	while (ft_strncmp((const char*)(tmp + 4), "IDAT", 4))
	{
		if (is_supp_anciliary_chunk(tmp) && !get_anciliary_info(img, tmp, fd))
			return (0);
		else if (!is_supp_anciliary_chunk(tmp) && !read_for_nuthin(fd, chunk_len
(tmp)))
			return (0);
		if (!crc_check(fd, tmp) || read(fd, tmp, 8) < 8)
			return (0);
	}
	if (ft_strncmp((const char*)(tmp + 4), "IDAT", 4) || !(zip = zipped_data(tmp
, fd, &comp_length)) || ft_strncmp((const char*)(tmp + 4), "IEND", 4
) || !(unzip_img_data(zip, comp_length, img)))
		return (0);
	ft_memdel((void**)&zip);
	close(fd);
	return (1);
}

SDL_Surface			*png2surf(char *path)
{
	int				fd;
	t_img			img;

	ft_bzero(&img, sizeof(img));
	if (((fd = open(path, (O_RDONLY | O_BINARY))) == -1) || !path)
		return (0);
	ft_putstr("Loading texture file ");
	ft_putendl(path);
	img = png_header_check(fd);
	if (img.err)
	{
		free_parse(&img);
		return (0);
	}
	if (!(img.surf = SDL_CreateRGBSurface(0, img.w, img.h, (img.depth * 4),
0x00FF0000, 0x0000FF00, 0x000000FF, 0xFF000000))
	|| !import_png_to_surface(fd, &img))
	{
		free_parse(&img);
		return (0);
	}
	free_parse(&img);
	return (img.surf);
}
