#include "png.h"
#include <stdio.h>
#include <fcntl.h>
#include <io.h>

#define ERR(x) {printf("ERR : %s \n", x);return (0);}

int main(int ac, char **av)
{
    SDL_Window *w;
    SDL_Renderer *r;
    SDL_Surface *s;
    SDL_Texture *t;
    SDL_Event e;

    int run;

    run = 1;
    printf("MICHEL ac %d av1 %s \n", ac, av[1]);
    if (ac != 2 || !av[1] || !av[1][0])
        ERR("NON")
    if (SDL_Init(SDL_INIT_VIDEO))
        ERR((const char*)SDL_GetError())
    if (!(w = SDL_CreateWindow("build", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1200, 800, SDL_WINDOW_SHOWN)))
		ERR((const char*)SDL_GetError())
    if (!(r = SDL_CreateRenderer(w, -1, SDL_RENDERER_SOFTWARE)))
		ERR((const char*)SDL_GetError())
    if (!(s = png2surf(av[1])))
        ERR("POURRIE TON IMAGE")
    if (!(t = SDL_CreateTextureFromSurface(r, s)))
        ERR("CANT CONVERT TO TXTRZ")
        
    SDL_RenderCopy(r, t, NULL, &(SDL_Rect){.x = 50, .y = 50, .w = 640, .h = 640});
    while (run)
    {
        while (SDL_PollEvent(&e) != 0)
	    { 
            if (e.type == SDL_KEYUP)
            {
                if (e.key.keysym.sym == SDLK_ESCAPE)
                    run = 0;
            }
            else if (e.type == SDL_QUIT)
                run = 0;
        }
        SDL_RenderPresent(r);
    }

    if (s)
		SDL_FreeSurface(s);
    if (r)
		SDL_DestroyRenderer(r);
	if (w)
		SDL_DestroyWindow(w);
    SDL_Quit();
    return (0);
}