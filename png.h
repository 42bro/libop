/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/23 03:07:18 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/23 15:45:30 by fdubois          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PNG_H
# define PNG_H

# include "../libft/libft.h"
# include <stdlib.h>
# include <SDL2/SDL.h>
# include <zlib.h>
# include <fcntl.h>
# include <math.h>

# include <stdio.h>

# define MAX_IDAT_SIZE	1048576
# define MAX_CHUNK_SIZE	16384

# define HAS_SUB		(1 << 1) /* internal flag for treating interlaced images */
# define HAS_UP			(1 << 2) /* internal flag for treating interlaced images */
# define HAS_GAMA		(1 << 3)
# define HAS_TRNS		(1 << 4)


typedef struct	s_filter
{
	uint64_t	sub;
	uint64_t	up;
}				t_filter;

typedef struct	s_img
{
	int			*plte;
	SDL_Surface	*surf;
	t_filter	f;
	size_t		usize;
	uint32_t	trns;
	uint32_t	gama;
	uint32_t	physx;
	uint32_t	physy;
	uint32_t	w;
	uint32_t	h;
	uint32_t	linesize;
	uint32_t	fd;
	uint8_t		plte_nb;
	uint8_t		depth;
	uint8_t		color_type;
	uint8_t		compression;
	uint8_t		filter;
	uint8_t		interlaced;
	uint8_t		channels;
	uint8_t		bpp;
	uint8_t		chunkflags;
	uint8_t		phys_spec;
	uint8_t		err;
}				t_img;

void			unfilter(unsigned char *str, int i, t_img *img
				, uint8_t filter_type);
int				read_extra_chunk(unsigned char *tmp, int fd);
void			sub_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			up_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			average_unfilter(unsigned char *str, uint32_t i, t_img *img);
void			paeth_unfilter(unsigned char *str, uint32_t i, t_img *img);
uint32_t		chunk_len(unsigned char *tmp);
int				crc_check(int fd, unsigned char *tmp);
int				read_for_nuthin(int fd, uint32_t len);
t_img			png_header_check(int fd);
t_img			get_png_header_info(int fd, int len);
void			free_parse(t_img *img);
int				get_gama_info(t_img *img, unsigned char *tmp, int fd);
int				get_phys_info(t_img *img, unsigned char *tmp, int fd);
int				get_palette_info(t_img *img, unsigned char *str, int fd);
int				invalid_trns_chunk(t_img *img, int len);
int				get_trns_info(t_img *img, unsigned char *tmp, int fd);
int				is_supp_anciliary_chunk(unsigned char *tmp);
int				get_anciliary_info(t_img *img, unsigned char *tmp, int fd);
unsigned char	*inflator(unsigned char *raw, uint32_t chunk_len, size_t usize);
int				unzip_img_data(unsigned char *raw_data, uint32_t chunk_len
				, t_img *img);
unsigned char	*zipped_data(unsigned char *tmp, int fd, uint32_t *comp_length);
void			put_pixel(uint32_t	*pixel, unsigned char *str, size_t k
				, t_img *img);
SDL_Surface		*png2surf(char *path);

#endif
