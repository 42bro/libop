/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_utils2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 15:52:02 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/22 17:13:52 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "png.h"

int		get_gama_info(t_img *img, unsigned char *tmp, int fd)
{
	if (chunk_len(tmp) != 4 || read(fd, tmp, 4) != 4)
		return (0);
	img->gama = chunk_len(tmp);
	img->chunkflags |= HAS_GAMA;
	return (1);
}

int		get_phys_info(t_img *img, unsigned char *tmp, int fd)
{
	unsigned char	p[9];

	if (chunk_len(tmp) != 9)
		return (0);
	if (read(fd, p, 9) != 9)
		return (0);
	img->physx = chunk_len(p);
	img->physy = chunk_len(p + 4);
	img->phys_spec = *(p + 8);
	return (1);
}

int		is_supp_anciliary_chunk(unsigned char *tmp)
{
	return (!ft_strncmp((const char*)(tmp + 4), "gAMA", 4) || !ft_strncmp((const
char*)(tmp + 4), "PLTE", 4) || !ft_strncmp((const char*)(tmp + 4), "tRNS", 4
) || !ft_strncmp((const char*)(tmp + 4), "pHYs", 4));
}

int		get_anciliary_info(t_img *img, unsigned char *tmp, int fd)
{
	if (!ft_strncmp((const char*)(tmp + 4), "gAMA", 4))
	{
		if (!get_gama_info(img, tmp, fd))
			return (0);
	}
	else if (!ft_strncmp((const char*)(tmp + 4), "PLTE", 4))
	{
		if (!get_palette_info(img, tmp, fd))
			return (0);
	}
	else if (!ft_strncmp((const char*)(tmp + 4), "tRNS", 4))
	{
		if (!get_trns_info(img, tmp, fd))
			return (0);
		img->chunkflags |= HAS_TRNS;
	}
	else if (!ft_strncmp((const char*)(tmp + 4), "pHYs", 4))
	{
		if (!get_phys_info(img, tmp, fd))
			return (0);
	}
	return (1);
}
