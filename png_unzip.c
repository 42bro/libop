/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_unzip.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/20 16:08:00 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/22 17:13:32 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "png.h"

unsigned char *inflator(unsigned char *raw, uint32_t chunk_len, size_t usize)
{
	unsigned char *str;
	z_stream z;

	if (!(str = (unsigned char *)malloc(sizeof(unsigned char) * (usize + 1))))
		return (NULL);
	z.zalloc = Z_NULL;
	z.zfree = Z_NULL;
	z.opaque = Z_NULL;
	z.avail_in = chunk_len;
	z.next_in = (Bytef *)raw;
	z.avail_out = usize;
	z.next_out = (Bytef *)str;
	inflateInit2_(&z, 15, ZLIB_VERSION, sizeof(z));
	inflate(&z, Z_NO_FLUSH);
	inflateEnd(&z);
	return (str);
}

void adam7_deinterlace(t_img *img, size_t *i, unsigned char *str)
{
	int x;
	int y;
	int pass;
	char filter;

	/* Pass  1  2  3  4  5  6  7  */
	static const char adam[4][7] = {{0, 0, 4, 0, 2, 0, 1},	/* y   */
									{0, 4, 0, 2, 0, 1, 0},	/* x   */
									{8, 8, 8, 4, 4, 2, 2},	/* y++ */
									{8, 8, 4, 4, 2, 2, 1}}; /* x++ */

	pass = -1;

	printf("CA DESENTRELACE \n");
	y = 0;
	img->chunkflags &= ~HAS_UP;
	while (++pass < 7)
	{
		y = adam[0][pass];
		while (y < (int)img->h)
		{
			img->chunkflags &= ~HAS_SUB;
			x = adam[1][pass];
			filter = str[(*i)];
			(*i)++;
			while (x < (int)img->w)
			{
				//printf("filter %d x %d y %d\n", (int)filter, x, y);
				unfilter(str, (int)(*i), img, filter);
				put_pixel((uint32_t *)(img->surf->pixels), str + (*i), (y * img->w + x), img);
				if (img->chunkflags & HAS_SUB)
					img->f.sub = (y * img->w + x);
				if (pass)
					img->f.up = (adam[0][pass - 1] + y * adam[2][pass - 1] + adam[1][pass - 1] + x * adam[3][pass - 1]);
				x += adam[3][pass];
				(*i) += img->bpp;
				img->chunkflags |= HAS_SUB;
			}
			y += adam[2][pass];
			//printf("coucou y %d x %d pass %d, %d, %d \n", y, x, pass, img->h, img->w);
		}
		img->chunkflags |= HAS_UP;
	}
}

int unzip_img_data(unsigned char *raw_data, uint32_t chunk_len, t_img *img)
{
	size_t i;
	int k;
	unsigned char *str;
	char filter;

	if (!(str = inflator(raw_data, chunk_len, img->usize)))
		return (0);
	i = 0;
	k = -1;
	if (img->interlaced)
		adam7_deinterlace(img, &i, str);
	else
	{
		while (i < img->usize)
		{
			if (((!(i % img->linesize)) && str[i] > 4))
				return (0);
			if (!(i % img->linesize))
				filter = str[i];
			else
			{
				unfilter(str, (int)i, img, filter);
				put_pixel((uint32_t *)(img->surf->pixels), str + i, ++k, img);
			}
			i += (i % img->linesize ? img->bpp : 1);
		}
	}
	ft_memdel((void **)&str);
	return (1);
}

unsigned char *zipped_data(unsigned char *tmp, int fd, uint32_t *comp_length)
{
	unsigned char *str;
	uint32_t len;
	unsigned char *data;
	int i;

	if (!(str = (unsigned char *)ft_strnew(MAX_IDAT_SIZE)))
		return (NULL);
	i = 0;
	while (!ft_strncmp((const char *)(tmp + 4), "IDAT", 4) && (len = chunk_len(tmp)))
	{
		if (!(data = (unsigned char *)malloc(sizeof(unsigned char) * (len + 1))))
			return (NULL);
		*comp_length += len;
		if (*comp_length >= MAX_IDAT_SIZE || read(fd, data, len) != (int32_t)len)
			return (NULL);
		ft_memcpy(str + i, data, len);
		if (!(i += len) || !crc_check(fd, tmp) || read(fd, tmp, 8) != 8)
			return (NULL);
		ft_memdel((void **)&data);
	}
	while (ft_strncmp((const char *)(tmp + 4), "IEND", 4))
		if (!read_extra_chunk(tmp, fd))
			return (NULL);
	return (str);
}

void put_pixel(uint32_t *pixel, unsigned char *str, size_t k, t_img *img)
{
	if (img->color_type == 2)
	{
		pixel[k] = (str[0] << 16) | (str[1] << 8) | (str[2]);
		pixel[k] |= (img->chunkflags & HAS_TRNS && pixel[k] == img->trns ? (0 << 24) : (255 << 24));
	}
	if (img->color_type == 3)
		pixel[k] = img->plte[str[0]];
	else if (img->color_type == 6)
		pixel[k] = (str[3] << 24) | (str[0] << 16) | (str[1] << 8) | (str[2]);
}

int get_palette_info(t_img *img, unsigned char *str, int fd)
{
	unsigned char rgb[4];
	int len;
	int i;

	len = chunk_len(str);
	i = -1;
	if (len % 3)
		return (0);
	if (!(img->plte = (int *)malloc(sizeof(int) * (len / 3))))
		return (0);
	img->plte_nb = len / 3;
	while (++i < (len / 3))
	{
		if (read(fd, rgb, 3) != 3)
			return (0);
		img->plte[i] = (rgb[0] << 16) | (rgb[1] << 8) | (rgb[2]);
	}
	return (1);
}
