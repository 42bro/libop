# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: fdubois <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/10/15 15:37:29 by fdubois           #+#    #+#              #
#    Updated: 2020/01/06 18:04:41 by fdubois          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = libop.a

CFLAGS = -Wall -Wextra -Werror

SRCS = png.c png_filters.c png_unzip.c png_utils.c png_utils2.c png_utils3.c

OBJS = $(SRCS:%.c=%.o)

INCS = png.h

LIBS = -lm -lz -lft

all: $(NAME)

$(OBJS): %.o : %.c $(INCS)
	$(CC) $(CFLAGS) -c $< -o $@

$(NAME): $(OBJS)
	ar -rc $@ $^

clean:
	rm -f $(OBJS)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re
