/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   png_utils3.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: fdubois <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/02 17:32:26 by fdubois           #+#    #+#             */
/*   Updated: 2019/06/22 17:13:59 by almoraru         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "png.h"

void	free_parse(t_img *img)
{
	if (img->plte)
		ft_memdel((void*)&(img->plte));
	close(img->fd);
}

void	unfilter(unsigned char *str, int i, t_img *img, uint8_t filter_type)
{
	if (filter_type == 1)
		if (img->interlaced && !(img->chunkflags & HAS_SUB))
			return;
		else
			sub_unfilter(str, i, img);
	else if (filter_type == 2)
		up_unfilter(str, i, img);
	else if (filter_type == 3)
		average_unfilter(str, i, img);
	else if (filter_type == 4)
		paeth_unfilter(str, i, img);
}

int		read_extra_chunk(unsigned char *tmp, int fd)
{
	if (!(read_for_nuthin(fd, chunk_len(tmp))))
		return (0);
	if (!crc_check(fd, tmp) || read(fd, tmp, 8) != 8)
		return (0);
	return (1);
}
